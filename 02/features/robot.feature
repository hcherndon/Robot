Feature: Tickle a happy robot

  Scenario: Tickle a happy robot
    Given I am in a good mood
    When you tickle me
    Then I will giggle

  Scenario: Kick a happy robot
    Given I am in a good mood
    When you kick me
    Then I will cry

  Scenario: Kick an angry robot
    Given I am in a angry mood
    When you kick me
    Then I will *dead*

  Scenario: Tickle an angry robot
    Given I am in a angry mood
    When you tickle me
    Then I will giggle
