package robot_assignment.implementation;

/***
 * Chapter 3 Robot assignment
 * Brent Reeves
***/
public class Robot {
    private String mood = "happy";
    private String recentAction = "";

    public void setMood(String var1) {
        this.mood = var1;
    }

    public String getMood() {
        return mood;
    }

    public void tickle() {
        this.recentAction = "tickle";
        this.mood = "good";
    }

    public void kick() {
        if (this.recentAction.equals("kick") || this.mood.equals("angry")) {
            this.recentAction = "kickAgain";
        } else {
            this.recentAction = "kick";
        }

        this.mood = "angry";
    }

    public String response() {
        if (this.mood.equals("good") && this.recentAction.equals("tickle")) {
            return "giggle";
        } else if (this.mood.equals("angry") && this.recentAction.equals("kick")) {
            return "cry";
        } else if (this.mood.equals("angry") && this.recentAction.equals("kickAgain")) {
            return "*dead*";
        } else {
            return "confuse";
        }
    }
}
