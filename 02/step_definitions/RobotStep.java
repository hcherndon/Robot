package robot_assignment.step_definitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import robot_assignment.implementation.Robot;

import static org.junit.Assert.assertEquals;

public class RobotStep {
    private Robot robot;

    @Before
    public void before() {
        robot = new Robot();
    }

    @Given("^I am in a (.*?) mood$")
    public void iAmInAMood(String s) throws Throwable {
        robot.setMood(s);
    }

    @When("^you (.*?) me$")
    public void youActMe(String action) throws Throwable {
        if (action.equals("kick")) {
            robot.kick();
        } else if (action.equals("tickle")) {
            robot.tickle();
        } else {
            assertEquals(false, true);
        }
    }

    @Then("^I will (.*?)$")
    public void iWillResponse(String s) throws Throwable {
        assertEquals(s, robot.response());
    }
}
